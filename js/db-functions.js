const Sequelize = require('sequelize');
const sql = new Sequelize('cards', 'tb', 'tbpass', {
  host: 'localhost',
  dialect: 'sqlite',
  operatorAliases: false,
  pool: {
    max: 30,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  storage: './cards.sqlite'
});

const crapData = sql.define('crap', {
  smell: {
    type: Sequelize.STRING
  },
  val: {
    type: Sequelize.STRING
  }
});

exports.test = function() {
  sql.authenticate().then(auth => {
    console.log("online");
  }).catch(err => {
    console.log("oh naw");
  }).done();
};

exports.checkdb = function() {
  crapData.findAll().then(crap => {
    crap.dataValues.foreach(function(val) {
      console.log(val.get("smell"));
      console.log(val.get("val"));
    });
  });
};

exports.singleCrap = function() {
  return crapData.findOne({
    where: {
      id: 4
    }
  });
};

exports.runOnce = function() {

  crapData.create({
    smell: 'lame',
    val: 'poo'
  });
  crapData.create({
    smell: 'dubious',
    val: 'ick'
  });
  crapData.create({
    smell: 'nada',
    val: 'solid'
  });
  crapData.create({
    smell: 'uhhh',
    val: 'gaseous'
  });
  crapData.create({
    smell: 'dinner',
    val: 'don\'t look'
  });
};

/*crapData.create({
    smell: 'dubious', 
    val: 'icky'
  })
  .then( () => 
    crapData.findOrCreate({
      where: {
        smell: 'dubious'
      },
      defaults: {
        val: 'splat'
      }
    })
  .spread((crap, created) => {
    console.log(crap.get({plain: true}));
    console.log(created);
  });*/