var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var app = express();
var reactViews = require('express-react-views');

app.set('port', process.env.PORT || 3000);
app.set('views', './views');
app.set('view engine', 'jsx');
app.engine('jsx', reactViews.createEngine());
//app.use(express.static(path.join('./')));

app.get('/', routes.Index);

http.createServer(app).listen(app.get('port'), function() {
  console.log('SERVER ON -- ' + app.get('port'));
});